# Code Notes

- [Code Notes](#code-notes)
  - [Programming](#programming)
    - [Python](#python)
    - [HTML](#html)
  - [Frameworks](#frameworks)
    - [Flask](#flask)
  - [Tools](#tools)
    - [Git](#git)

## Programming

### Python

- [DSP](./topics/Python/DSP.md)
- [Virtual envs](topics/Python/VirtualEnv.md)
- [with](./topics/Python/with.md)

### HTML

- [HTML Basics](topics/HTML/Basics.md)

## Frameworks

### Flask

- [FLask Basics](./topics/Flask/Basics.md)
- [Minimal app](https://flask.palletsprojects.com/en/2.2.x/quickstart/)
- [Routing](topics/Flask/Routing.md)
- [Redirect](./topics/Flask/Redirect.md)
- [Context](https://flask.palletsprojects.com/en/2.2.x/appcontext/)

## Tools

### Git

- [Git Basics](./topics/Git/Basics.md)
- [Git cheatsheet Markdown](./topics/Git/Cheatsheet_1.md)
- [Github Git cheatsheet PDF](./topics/Git/pdf/git-cheat-sheet-education.pdf)
- [Gitlab Git cheatsheet](./topics/Git/pdf/gitlab-git-cheat-sheet.pdf)
- [Gitlab Git cheatsheet commands](./topics/Git/Cheatsheet_Git_commands.md)
- [Git Advance](./topics/Git/Advance.md)
- [Version Control Service (VCS)](./topics/Git/VCS.md)
- [Undoing](./topics/Git/Undoing.md)
- [gitignore](./topics/Git/gitignore.md)
