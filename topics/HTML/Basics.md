# HTML basics

[**Back to index**](../../README.md)

## Make a button to navigate

<https://www.scaler.com/topics/how-to-make-a-button-link-to-another-page-in-html/>

```html
 <button onclick = "window.location.href='www.linktothepage.com';"
```

## Play audio in HTML

<https://www.w3schools.com/html/html5_audio.asp>

## Use custom fonts

<https://www.youtube.com/watch?v=AAU25Fo4bFY&ab_channel=iEatWebsites>

To use a custom font globally in HTML, you can use the @font-face rule in your CSS to specify the font file and its name, and then apply the font to the body or other elements on your page. Here's an example code snippet:

```html
<!DOCTYPE html>
<html>
<head>
 <title>My Web Page</title>
 <style>
  @font-face {
   font-family: 'MyCustomFont';
   src: url('path/to/mycustomfont.woff2') format('woff2'),
     url('path/to/mycustomfont.woff') format('woff');
   font-weight: normal;
   font-style: normal;
  }
  
  body {
   font-family: 'MyCustomFont', sans-serif;
  }
 </style>
</head>
<body>
 <h1>Welcome to my web page</h1>
 <p>This is an example of how to use a custom font globally in HTML.</p>
 <p>All text on this page will be displayed in the custom font specified by the 'MyCustomFont' name, or a similar sans-serif font if the custom font is not available on the user's device.</p>
</body>
</html>
```

## Change button color

To change the font of a button in HTML, you can use the CSS font-family property.

```html
<button style="font-family: Arial; color: white; background-color: blue;">Click me!</button>
```

You can also define the CSS in a separate CSS file or section using a class or id:

```html
<button class="myButton">Click me!</button>
```

```css
.myButton {
  font-family: Arial;
  color: white;
  background-color: blue;
}
```
