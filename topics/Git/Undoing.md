# Undo Git

[**Back to index**](../../README.md)

### Revert modified files before they get stage

This command "checking out" the snapshot of the original file. Useful when I have modified some file and I wanna restore it from the repository

```bash
git checkout filename
```

### Check change by change if you want to go back to the previous snapshot or not

```bash
git checkout -p 
```

### Add all the files modified into the stage area in one step

> git add *

* example:

1) create a temporary file to debugging (for example)

```bash
./filename.py > temporary_filename
```

2) add all the files modified (which are not in the stage area)

```bash
git add *
```

* But the HEAD can be reset reset command MOVE out files from the stage area

```bash
git reset HEAD temporary_filename
```

now, the temporary_filename is untracked & no longer stage

---

### Amending Commits

--amend:\
change an error in the stage that we were working (I mean, the previous commit)

git commit --ammend

### Roll backs

it creates a commit that contains the inverse of all the changes made in the. Bad commit in order to cancel them out. Since we can think of head as a pointer to the snapshot of your current commit, when we pass head to the revert command we tell Git.\
To rewind that current commit

```bash
git revert HEAD
```

it could be good add an explaination why I've reverted the commit

### Identify a commit

```bash
git show commit_hash
```

example:

```bash
git show 3202f9e04de296e9a1a442ac228474ca5c2ca935
````

**git show 3202f will work to!**
