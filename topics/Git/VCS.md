# Version Control Service (VCS)

[**Back to index**](../../README.md)

### Push a local repository to Github / Gitlab

```bash
git init
git add .
git commit -m "Initial commit"

git remote add origin [GitLab repository URL]
git push -u origin main
```

### Create a repository on Github on a especific adress

```bash
git clone repository_adress
git clone repository_adress adress_path
```

Make changes into the files & commit them.\
PUSH the commited files to github

```bash
git push
```

### To avoid enter user + password on every push (only 15 minutes)

`git config --global credential.helper cache`
