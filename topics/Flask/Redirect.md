
## Redirect

```python
from flask import Flask, redirect, url_for

app = Flask(__name__)

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login')
def login():
    return 'Login Page'

if __name__ == '__main__':
    app.run()
```

In this example, the index function redirects the user to the login function using the redirect function and passing in the URL of the login function obtained from the url_for function. The url_for function takes the name of the function as an argument and returns the URL for that function.

You can also redirect to an external URL by passing the URL as a string to the redirect function:

```python
from flask import Flask, redirect

app = Flask(__name__)

@app.route('/')
def index():
    return redirect('https://www.google.com')

if __name__ == '__main__':
    app.run()
```
